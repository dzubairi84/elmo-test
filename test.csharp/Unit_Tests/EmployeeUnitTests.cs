﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVC40_Knockout_App.Services;
using MVC40_Knockout_App.Models;
using System.Linq;

namespace Unit_Tests
{
    [TestClass]
    public class EmployeeUnitTests
    {
        [TestMethod]
        public void TestLeaveApplicationView()
        {
            LeaveApplicationService service = new LeaveApplicationService();

            var result = service.LeaveApplicationViews();

            Assert.AreEqual(5, result.ToList().Count);
        }
    }
}

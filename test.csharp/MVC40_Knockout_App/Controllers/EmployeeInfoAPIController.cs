﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MVC40_Knockout_App.Models;
using MVC40_Knockout_App.Services;

namespace MVC40_Knockout_App.Controllers
{
    /// <summary>
    /// EmployeeInfoAPIController is used to manage the interactions
    /// between the presentation and the application layer
    /// </summary>
    public class EmployeeInfoAPIController : ApiController
    {

        EmployeeInfoService service = new EmployeeInfoService();
        // GET api/EmployeeInfoAPI
        public HttpResponseMessage GetEmployeeInfoes()
        {
            // INTERVIEW_TODO: 1. Return a list of Employees
            // Details: 
            // Modify this method to return a HttpResponseMessage with status code 200 and the serialised list of employees
            // in the response body.
            //
            // Question 1: 
            // This application is using a layered architecture with an API Controller that is dependant on the 
            // concrete implementation of EmployeeInfoService, a service that is re-created each time the API controller is instantiated.
            // In a production application, how would the service be instantiated, and how would data persistence be achieved?
            // Question 2: 
            // What are some of the anti-patterns apparent in this design? 
            HttpResponseMessage response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Not implemented!");
            return response;
        }

        //GET api/EmployeeInfoAPI/5
        public HttpResponseMessage GetEmployeeInfo(int id)
        {
            // INTERVIEW_TODO: 2. Return the Employee or a Not Found (404) response if the ID is not valid
            // Details:
            // Use the  EmployeeInfoService to find the employee, and return one of the following:
            // The full EmployeeInfo object if it exists OR HttpStatusCode.NotFound
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Not implemented!");
        }

        // PUT api/EmployeeInfoAPI/5
        public HttpResponseMessage PutEmployeeInfo(int id, EmployeeInfo employeeinfo)
        {
            // INTERVIEW_TODO: 3. Implement the UpdateEmployee method of the employee service
            // Details: 
            // The previous developer didn't implement the UpdateEmployee function,
            // modify the existing UpdateEmployee method to find the employee, and update the record.
            // Question 3: How do we ensure that the employee exists, and what should we do if they don't?
            // Question 4: What unit tests would you implement to cover the UpdateEmployee method?
            if (ModelState.IsValid && id == employeeinfo.EmpNo)
            {
                service.UpdateEmployee(id, employeeinfo);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/EmployeeInfoAPI
        public HttpResponseMessage PostEmployeeInfo(EmployeeInfo employeeinfo)
        {
            // : 4. Validate the employee, and return a meaningful error if it is not valid
            // Details:
            // We need to modify the application to ensure that all employees have salary > 0, and a unique EmpNo
            // The user should be notified with a meaningful error message if the request is not valid.
            // The check should be done in the presentation as well as the business layer.
            // Please modify the Create.cshtml file (it contains all the javascript code) to handle the validation logic.
            // Question 5: There are several approaches to solve this, what approach would you recommend for a production application?
            // Question 6: Can you suggest any application UI changes to improve the user interaction?
            if (ModelState.IsValid)
            {
                service.InsertEmployee(employeeinfo);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, employeeinfo);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = employeeinfo.EmpNo }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/EmployeeInfoAPI/5
        public HttpResponseMessage DeleteEmployeeInfo(int id)
        {
            service.DeleteEmployee(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}